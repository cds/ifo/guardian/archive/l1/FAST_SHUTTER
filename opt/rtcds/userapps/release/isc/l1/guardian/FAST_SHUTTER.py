#! /usr/bin/env python

from guardian import GuardState, GuardStateDecorator
import time

request = 'DOWN'
nominal = 'DOWN'

###############################################
# reference values for 
###############################################
PdAChannel = 'ASC-AS_A_DC_SUM_OUTPUT'
PdARelGain = 58500 #LHO 247500.0 # gain of PdA relative to trigger PD (H1:SYS-MOTION_C_SHUTTER_G_TRIGGER_POWER)
PdADarkLo = -15       # dark level low
PdADarkHi = 15       # dark level high
PdBChannel = 'ASC-AS_B_DC_SUM_OUTPUT'
PdBRelGain = 9650.0 #LHO 247500.0 # gain of PdB relative to trigger PD (H1:SYS-MOTION_C_SHUTTER_G_TRIGGER_POWER)
PdBDarkLo = -15       # dark level low
PdBDarkHi = 15       # dark level high
###############################################

class INIT(GuardState):
    index = 0
    def main(self):
        return True


class DOWN(GuardState):
    index = 10
    def main(self):
        return True
        # Hang out here when IFO is unlocked.  Lockloss returns here
        # Make sure shutter is closed


class TEST_SHUTTER(GuardState):
    index = 18
    redirect = False # Must finish this whole state before going on

    def main(self):
        # calculate PDA and PDB tresholds
        triglo = ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_LOW']
        trighi = ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_HIGH']
        self.PdAPowLo = triglo*PdARelGain
        self.PdAPowHi = trighi*PdARelGain*2
        self.PdBPowLo = triglo*PdBRelGain
        self.PdBPowHi = trighi*PdBRelGain*2
        log('Thresholds for '+PdAChannel+' are '+str(self.PdAPowLo)+' (low), '+str(self.PdAPowHi)+' (high)')
        log('Thresholds for '+PdBChannel+' are '+str(self.PdBPowLo)+' (low), '+str(self.PdBPowHi)+' (high)')
        self.attempts = 0
        self.success = False
        self.testRunning = False
        self.first_time = False

    def run(self):
        if self.success:
            return True
        if self.testRunning:
            #JCB 2022-05-12 open the shutter after 3 seconds, but only once
            if self.timer['ShutterGReopen'] and self.first_time:
                ezca['SYS-MOTION_C_SHUTTER_G_OPEN'] = 1
                self.first_time = False
            #log('Testing Fast SHutter: PDA power = '+str(ezca[PdAChannel])+' PDB power = '+str(ezca[PdBChannel]))
            if ezca['SYS-PROTECTION_AS_STATUS'] == 3:
                notify('Fast shutter testing in progress')   
            elif ezca['SYS-PROTECTION_AS_STATUS'] == 1:
                notify('Fast shutter failed tests!!')
                self.testRunning = False
                return False
                #return 'SHUTTER_FAILURE'
            elif ezca['SYS-PROTECTION_AS_STATUS'] == 0:
                log('Shutter tests okay.  Continue locking')
                self.success = True
        else:
            time.sleep(1)
            self.attempts = self.attempts + 1
            if self.attempts < 3:
                # If fast shutter is closed - open it
                log('Checking if shutter open')
                if (ezca['SYS-PROTECTION_AS_FASTSHUTTERREADY'] == 0):
                    ezca['ISI-HAM6_SATCLEAR'] = 1
                    if (ezca['SYS-MOTION_C_SHUTTER_G_STATE'] == 1):
                        log('Reseting trigger')             
                        ezca['SYS-MOTION_C_SHUTTER_G_OPEN'] = True
                        time.sleep(0.5)
                    # open shutter
                    log('Reseting Shutter')
                    ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = True
                    time.sleep(0.5)
                elif (ezca['SYS-MOTION_C_SHUTTER_G_STATE'] == 1):
                        log('Reseting trigger but not FAST_SHUTTER')             
                        ezca['SYS-MOTION_C_SHUTTER_G_OPEN'] = True
                        time.sleep(0.5)

                # check the downrange (ASC-AS_A_DC_SUM_OUT_DQ and ASC-AS_B_DC_SUM_OUT_DQ) power levels
                pdApow = ezca[PdAChannel]
                pdBpow = ezca[PdBChannel]
                if (pdApow < self.PdAPowLo) or (pdApow > self.PdAPowHi) or (pdBpow < self.PdBPowLo) or (pdBpow > self.PdBPowHi) :
                    ezca['SYS-PROTECTION_AS_SETTOFAULT'] = 1
                    notify('Fast shutter failed tests!! Downrange power out of range when open.')
                    return False
                    #return 'SHUTTER_FAILURE'

                #clear saturations on HAM6 before triggering shutter
                ezca['ISI-HAM6_SATCLEAR'] = 1
                time.sleep(0.2)
                # Click "run" button on AS Port Protection screen to run tests
                ezca['SYS-PROTECTION_AS_RUNTEST'] = 1
                #JCB 2022-05-12 open the shutter after 3 seconds
                self.timer['ShutterGReopen'] = 3
                self.first_time = True
                time.sleep(0.5) # wait just a little bit for the shutter to trigger

                # check the downrange (ASC-AS_A_DC_SUM_OUT_DQ and ASC-AS_B_DC_SUM_OUT_DQ) power levels - need to be dark
                #we only need to do this check once, there is no need for it to fail if the light was gone and the shutter opens
                pdApow = ezca[PdAChannel]
                pdBpow = ezca[PdBChannel]
                if (pdApow < PdADarkLo) or (pdApow > PdADarkHi) or (pdBpow < PdBDarkLo) or (pdBpow > PdBDarkHi) :
                    ezca['SYS-PROTECTION_AS_SETTOFAULT'] = 1
                    notify('Fast shutter failed tests!! Downrange light does not disappear properly!')
                    return False
                    #return 'SHUTTER_FAILURE'
                self.testRunning = True
            else:
                return 'SHUTTER_FAILURE'

class CHECK_READY(GuardState):
    index = 20
    redirect = False # Must finish this whole state before going on
    def main(self):
        # Do tests, return Ready

        # initialize variables
        self.shutter_ready = False
        self.trigger_power_okay = False
        self.no_err_msgs = False

        # If fast shutter is closed - open it
        if not ezca['SYS-PROTECTION_AS_FASTSHUTTERREADY']:
            # open shutter
            ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = True
            time.sleep(0.5)

        # calculate PDA and PDB tresholds
        triglo = ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_LOW']
        trighi = ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_HIGH']
        self.PdAPowLo = triglo*PdARelGain
        self.PdAPowHi = trighi*PdARelGain
        self.PdBPowLo = triglo*PdARelGain
        self.PdBPowHi = trighi*PdARelGain
        log('Thresholds for '+PdAChannel+' are '+str(self.PdAPowLo)+' (low), '+str(self.PdAPowHi)+' (high)')
        log('Thresholds for '+PdBChannel+' are '+str(self.PdBPowLo)+' (low), '+str(self.PdBPowHi)+' (high)')

    def run(self):
        # Check if shutter HV thinks it's ready
        if (ezca['SYS-MOTION_C_FASTSHUTTER_A_READY'] and ezca['SYS-PROTECTION_AS_FASTSHUTTERREADY'] and ezca['SYS-PROTECTION_AS_LOWPOWERREADY']):
            self.shutter_ready = True
        else:
            notify('Fast Shutter is not ready, cannot proceed')
            self.shutter_ready = False

        # Is there actually light on the trigger PD?
        trigpow = ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_POWER']
        if (trigpow >= ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_LOW']) and (trigpow <= ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_HIGH']):
            self.trigger_power_okay = True
        else:
            notify('AS port power not within limits, cannot proceed.')
            self.trigger_power_okay = False

        #what is the point of this check??
        # check the downrange (ASC-AS_A_DC_SUM_OUT_DQ and ASC-AS_B_DC_SUM_OUT_DQ) power levels
        '''pdApow=ezca[PdAChannel]
        pdBpow=ezca[PdBChannel]
        if (pdApow < self.PdAPowLo) or (pdApow > self.PdAPowHi) or (pdBpow < self.PdBPowLo) or (pdBpow > self.PdBPowHi) :
            notify('Downrange power out of range when open, cannot proceed.')
            self.trigger_power_okay = False'''

        # Are there any error messages?
        if ezca['SYS-MOTION_C_SHUTTER_G_ERROR_FLAG'] or ezca['SYS-MOTION_C_FASTSHUTTER_A_ERROR_FLAG']:
            notify('Fast shutter has error flag, cannot proceed')
            self.no_err_msgs = False
        else:
            self.no_err_msgs = True

        # If everything okay, return True
        if self.shutter_ready and self.trigger_power_okay and self.no_err_msgs:
            return True
        else:
            pass
            #notify('Fast shutter not okay. Hold here')


class READY(GuardState):
    index = 21
    def main(self):
        #  reset HAM6 ISI saturations
        ezca['ISI-HAM6_SATCLEAR'] =1
    def run(self):
        return True
        # Empty state, to say when ready


class SHUTTER_FAILURE(GuardState):
    index = 1
    def main(self):
        #  reset HAM6 ISI saturations
        ezca['ISI-HAM6_SATCLEAR'] =1
    def run(self):
        notify('Fast shutter failed tests!  Do not power up!!')




edges = [
    ('INIT','DOWN'),
    ('READY','DOWN'),
    ('DOWN','TEST_SHUTTER'),
    ('TEST_SHUTTER', 'CHECK_READY'),
    ('CHECK_READY','READY'),
    ('TEST_SHUTTER','SHUTTER_FAILURE')
]
